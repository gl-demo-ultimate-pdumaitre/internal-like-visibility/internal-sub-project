# Internal sub-project


This project mimics the internal visibility from self-managed, for `gitlab.com` namespaces.

Visibility is set to public, and parent group is also set to public with a [restriction by domain](https://docs.gitlab.com/ee/user/group/access_and_permissions.html#restrict-group-access-by-domain).
